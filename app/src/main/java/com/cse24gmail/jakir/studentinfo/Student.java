package com.cse24gmail.jakir.studentinfo;

/**
 * Created by Jakir on 5/8/2015.
 */
public class Student {
    private String name;
    private String registrationNo;
    private String email;

    public Student(String name, String registrationNo, String email) {
        this.name = name;
        this.registrationNo = registrationNo;
        this.email = email;
    }

    public Student() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public String getEmail() {
        return email;
    }
}
