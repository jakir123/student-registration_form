package com.cse24gmail.jakir.studentinfo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Jakir on 5/8/2015.
 */
public class DBHelper extends SQLiteOpenHelper{

    private static final String DB_NAME="students_db";
    private static final int DB_VERSION=1;

    public static final String TABLE_STUDENTS="students";
    public static final String NAME_FIELD="name";
    public static final String REG_NO_FIELD="reg_no";
    public static final String EMAIL_FIELD="email";

    public static final String STUDENTS_TABLE_SQL="CREATE TABLE " + TABLE_STUDENTS + " ( "
            + NAME_FIELD + " text, " + REG_NO_FIELD + " text, " + EMAIL_FIELD + " text )";



    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(STUDENTS_TABLE_SQL);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
