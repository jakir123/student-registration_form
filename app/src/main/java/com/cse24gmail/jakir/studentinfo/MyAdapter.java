package com.cse24gmail.jakir.studentinfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Jakir on 5/8/2015.
 */
public class MyAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Student> studentsList;
    private LayoutInflater mInflater;

    public MyAdapter(Context context,ArrayList<Student> studentsList){
        this.context=context;
        this.studentsList=studentsList;
        mInflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return studentsList.size();
    }

    @Override
    public Object getItem(int position) {
        return studentsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        if(convertView==null){
            view=mInflater.inflate(R.layout.listview_sample,parent,false);
            TextView tvName= (TextView) view.findViewById(R.id.tvName);
            TextView tvRegNo= (TextView) view.findViewById(R.id.tvRegNo);
            TextView tvEmail= (TextView) view.findViewById(R.id.tvEmail);

            tvName.setText(studentsList.get(position).getName());
            tvRegNo.setText(studentsList.get(position).getRegistrationNo());
            tvEmail.setText(studentsList.get(position).getEmail());
        }else{
            view=convertView;
        }

        return view;
    }
}
