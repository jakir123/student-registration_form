package com.cse24gmail.jakir.studentinfo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by Jakir on 5/8/2015.
 */
public class DBAdapter {
    private DBHelper dbHelper;
    private SQLiteDatabase db;
    public DBAdapter(Context context){
        dbHelper=new DBHelper(context);
    }

    public void open(){
        db=dbHelper.getWritableDatabase();
    }
    public void close(){
        dbHelper.close();
    }

    public long addStudent(Student student){
        ContentValues values=new ContentValues();
        values.put(DBHelper.NAME_FIELD,student.getName());
        values.put(DBHelper.REG_NO_FIELD,student.getRegistrationNo());
        values.put(DBHelper.EMAIL_FIELD,student.getEmail());

        long inserted=db.insert(dbHelper.TABLE_STUDENTS,null,values);
        return inserted;
    }

    public ArrayList<Student> getStudentInfo(){
        ArrayList<Student> allStudents=new ArrayList<Student>();
        db=dbHelper.getReadableDatabase();

        Cursor cursor=db.query(DBHelper.TABLE_STUDENTS, null, null, null, null, null, null);
        if(cursor!=null && cursor.getCount()>0){
            cursor.moveToFirst();
            for(int i=0;i<cursor.getCount();i++){
                String name=cursor.getString(cursor.getColumnIndex(DBHelper.NAME_FIELD));
                String regNo=cursor.getString(cursor.getColumnIndex(DBHelper.REG_NO_FIELD));
                String email=cursor.getString(cursor.getColumnIndex(DBHelper.EMAIL_FIELD));

                Student student=new Student(name,regNo,email);
                allStudents.add(student);
                cursor.moveToNext();
            }
        }
        db.close();
        return allStudents;
    }


    public ArrayList<Student> getStudentInfoByName(String name){

        ArrayList<Student> students=new ArrayList<Student>();
        db=dbHelper.getReadableDatabase();
        String query="SELECT "+DBHelper.TABLE_STUDENTS+"."+DBHelper.NAME_FIELD+", "
                +DBHelper.TABLE_STUDENTS+"."+DBHelper.REG_NO_FIELD
                +", "+DBHelper.TABLE_STUDENTS+"."+DBHelper.EMAIL_FIELD
                +" FROM "+DBHelper.TABLE_STUDENTS
                +" WHERE "+DBHelper.TABLE_STUDENTS+"."+DBHelper.NAME_FIELD+" LIKE '%"+name+"%'";
        Cursor cursor=db.rawQuery(query,null);
        if(cursor!=null && cursor.getCount()>0){
            cursor.moveToFirst();
            for (int j=0;j<cursor.getCount();j++){
                String s_name = cursor.getString(cursor.getColumnIndex(DBHelper.NAME_FIELD));
                String regNo=cursor.getString(cursor.getColumnIndex(DBHelper.REG_NO_FIELD));
                String email=cursor.getString(cursor.getColumnIndex(DBHelper.EMAIL_FIELD));

                Student student = new Student(s_name,regNo,email);
                students.add(student);
                cursor.moveToNext();

            }
        }
        cursor.close();
        return students;

    }

}
