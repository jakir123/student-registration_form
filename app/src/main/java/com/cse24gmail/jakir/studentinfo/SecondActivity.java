package com.cse24gmail.jakir.studentinfo;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;


public class SecondActivity extends ActionBarActivity {

    EditText etSearchView;
    ListView lvStudentList;
    ArrayList<Student> studentsList;
    TextView tvAddNewStudent;
    DBAdapter dbAdapter;
    MyAdapter myListAdapter;
    static String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initialization();
        eventListener();
    }

    private void initialization() {
        etSearchView= (EditText) findViewById(R.id.etSearchView);
        lvStudentList= (ListView) findViewById(R.id.lvStudentList);
        tvAddNewStudent= (TextView) findViewById(R.id.tvAddNew);

        studentsList=new ArrayList<Student>();
        dbAdapter=new DBAdapter(this);
        studentsList=dbAdapter.getStudentInfo();
        myListAdapter=new MyAdapter(this,studentsList);
        lvStudentList.setAdapter(myListAdapter);


    }

    private void eventListener() {
        tvAddNewStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SecondActivity.this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        etSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                name = s.toString().trim();
                studentsList=dbAdapter.getStudentInfoByName(name);
                myListAdapter=new MyAdapter(SecondActivity.this,studentsList);
                lvStudentList.setAdapter(myListAdapter);
                myListAdapter.notifyDataSetChanged();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (id==R.id.search_menu){
            etSearchView.setVisibility(View.VISIBLE);
           if(etSearchView.getVisibility()==View.VISIBLE){
               etSearchView.setFocusable(true);
           }
        }

        return super.onOptionsItemSelected(item);
    }
}
